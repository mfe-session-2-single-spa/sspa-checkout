import { useEffect, useState } from "react";

import Typography from "@mui/material/Typography";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import { cartInfo$ } from "@acid/sspa-utility";

export default function Root(props) {
  
  const [cartItems, setCartItems] = useState([]);
  
  useEffect(() => {
    const sub = cartInfo$.subscribe(({items}) => {
      setCartItems(items);
    });
    
    return () => {
      sub.unsubscribe();
    };
  }, [cartItems]);
  
  
  return (
    <>
      <Typography variant="h4" gutterBottom>
        Products in your Cart
      </Typography>
      <List >
        {cartItems.reverse().map((itemId) => (
          <ListItem key={`product-${itemId}`}>
            <ListItemText primary={`Product-${itemId}`} />
          </ListItem>
        ))}
      </List>
    </>
  );
}
